package ufficiopostale;

public class Pila{

    private Nodo primo;
    private Nodo ultimo;
    private int dim = 0;

    public Pila() {
        this.primo = this.ultimo = null;
    }

    //Push
    public void push(Nodo n) {
        if (primo != null) {
            primo.setPrec(n);
        }

        n.setSucc(primo);
        n.setPrec(null);

        primo = n;
        if (ultimo == null) {
            ultimo = primo;
        }
        dim++;
    }

    //Pop
    public Nodo pop() {
        Nodo n = primo;

        if (primo.getSucc() != null) {
            primo.getSucc().setPrec(null);
        }

        primo = primo.getSucc();
        dim--;
        return n;
    }

    public Nodo getTesta() {
        return primo;
    }

    public void setTesta(Nodo testa) {
        this.primo = testa;
    }

    public Nodo getPila() {
        return primo;
    }

    /**
     * @param coda the coda to set
     */
    public void setCoda(Nodo coda) {
        this.ultimo = coda;
    }
    
    public String[] toArray() {
        int pos = 0;
        return toArray(new String[dim], getTesta(), pos);
    }
    private String[] toArray(String[] array, Nodo nodo, int pos) {
        System.out.println(nodo.getCognome() + " ("+nodo.getNcoda()+")");
        array[pos] = nodo.getCognome() + " ("+nodo.getNcoda()+")";
        if (nodo.getPrec() != null && pos <= dim) {
            return toArray(array, nodo.getPrec(), dim);
        } else {
            return array;
        }
    }
    
    @Override
    public String toString() {
        return "Pila{" + "primo=" + primo + ", ultimo=" + ultimo + '}';
    }
    

}
