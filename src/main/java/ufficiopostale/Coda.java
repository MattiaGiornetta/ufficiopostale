package ufficiopostale;

public class Coda {

    private Nodo primo;
    private Nodo ultimo;

    public Coda() {
        this.primo = this.ultimo = null;
    }

    //Push
    public void push(Nodo n) {
        if (primo != null) {
            primo.setPrec(n);
        }

        n.setSucc(primo);
        n.setPrec(null);

        primo = n;
        if (ultimo == null) {
            ultimo = primo;
        }
    }

    //Pop
    public Nodo pop() {
        Nodo n = ultimo;

        if (ultimo.getPrec() != null) {
            ultimo.getPrec().setSucc(null);
        }

        ultimo = ultimo.getPrec();

        return n;
    }

    public Nodo getTesta() {
        return primo;
    }

    public void setTesta(Nodo testa) {
        this.primo = testa;
    }

    public Nodo getCoda() {
        return ultimo;
    }

    /**
     * @param coda the coda to set
     */
    public void setCoda(Nodo coda) {
        this.ultimo = coda;
    }

    @Override
    public String toString() {
        return "Pila{" + "primo=" + primo + ", ultimo=" + ultimo + '}';
    }

}
