package ufficiopostale;


public class Nodo {

    /**
     * @return the prec
     */
    public Nodo getPrec() {
        return prec;
    }

    /**
     * @param prec the prec to set
     */
    public void setPrec(Nodo prec) {
        this.prec = prec;
    }
    
    private String cognome;
    private int Ncoda;
    private Nodo succ;
    private Nodo prec;

    public Nodo(int Ncoda, String nome) {
        this.Ncoda = Ncoda;
        this.cognome = nome;
        this.succ = null;
        this.prec = null;
    }

    @Override
    public String toString() {
        return "attesa{" + "cognome=" + cognome + ", Ncoda=" + Ncoda + '}';
    }
    
    public Nodo getSucc() {
        return succ;
    }

    
    public void setSucc(Nodo succ) {
        this.succ = succ;
    }

    /**
     * @return the nome
     */
    public String getCognome() {
        return cognome;
    }

    /**
     * @param cognome the nome to set
     */
    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    /**
     * @return the Ncoda
     */
    public int getNcoda() {
        return Ncoda;
    }

    /**
     * @param Ncoda the Ncoda to set
     */
    public void setNcoda(int Ncoda) {
        this.Ncoda = Ncoda;
    }





}
